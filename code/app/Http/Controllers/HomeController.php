<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Product;
use App\Models\Category;

class HomeController extends Controller
{

    protected $viewStyles = [
        Product::VIEW_STYLE_GROUPED,
        Product::VIEW_STYLE_UNGROUPED
    ];

    public function index(Request $request, Product $product)
    {
        $categories = Category::orderBy('category')->get();

        $productsViewStyle = $this->getProductsViewStyle();

        $products = $product->getProducts($productsViewStyle);

        return view('home.index', compact(['categories', 'products', 'productsViewStyle']));
    }

    public function store(Request $request)
    {
        request()->validate([
            'product' => 'required',
            'category_id' => 'required_unless:category,new',
            'category' => 'required_if:category_id,new'
        ]);

        if ($request->filled('category')) {
            $insertedCategoryData = Category::create($request->only(['category']));
            $request->merge(['category_id' => $insertedCategoryData->id]);
        }

        Product::create($request->except(['category']));

        return redirect()->route('index')
            ->with('success', 'Product added.');

    }

    public function deleteProduct(Request $request)
    {
        Product::destroy($request->id);

        return redirect()->route('index')
            ->with('success', 'Product deleted.');
    }

    public function deleteMultipleProducts(Request $request)
    {
        if(isset($request->productsIds)){
            foreach($request->productsIds as $productId){
                Product::destroy($productId);
            }
        }

        return redirect()->route('index')
            ->with('success', 'Multiple products deleted.');
    }

    public function setViewStyle(Request $request)
    {
        $this->setProductsViewStyle();

        return redirect()->route('index');
    }

    private function setProductsViewStyle()
    {
        $productsViewStyle = request()->productsViewStyle;

        if(isset($productsViewStyle) && in_array($productsViewStyle, $this->viewStyles)){
            request()->session()->put('products-view-style', $productsViewStyle);
        }
        return $this->getProductsViewStyle();
    }

    private function getProductsViewStyle(){
        return request()->session()->get('products-view-style', Product::VIEW_STYLE_UNGROUPED);
    }

}
