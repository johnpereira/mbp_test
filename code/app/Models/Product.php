<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    const VIEW_STYLE_GROUPED = 'grouped';
    const VIEW_STYLE_UNGROUPED = 'ungrouped';

    protected $fillable = [
        'product', 'category_id'
    ];

    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }

    public function getProducts($viewStyle)
    {
        if( $viewStyle == SELF::VIEW_STYLE_GROUPED ){
            return $this->getProductsGrouped();
        } else {
            return $this->getProductsUngrouped();
        }
    }

    private function getProductsUngrouped()
    {
        return $this->orderBy('product')->get();
    }

    private function getProductsGrouped()
    {
        $products = $this->orderBy('product')->get();

        $groupedProducts = $products->mapToGroups(function($item){
            return [$item->category->category => $item];
        });

        $groupedProducts = $groupedProducts->sortKeys();

        return $groupedProducts;
    }

}