@extends('layouts.master')

@section('content')

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('index') }}">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Categories</li>
        </ol>
    </nav>

    <a href="{{ route('categories.create') }}" class="btn btn-success my-4">Add a category</a>

    @if($categories->isNotEmpty())

        <table class="table">
            <thead>
            <tr>
                <th colspan="2">Category (products)</th>
            </tr>
            </thead>
            <tbody>
                @foreach($categories as $category)
                    <tr>
                        <td>{{ $category->category }} ({{ $category->products->count() }})</td>
                        <td class="text-right">
                            <form action="{{ route('categories.destroy', $category->id) }}" method="POST">
                                <a class="btn btn-outline-primary" href="{{ route('categories.edit', $category->id) }}"><i class="far fa-edit"></i></a>
                                @csrf
                                @method('DELETE')
                                <button class="confirm btn btn-outline-danger" type="submit" data-confirm-text="Are you sure? Delete the category and its products."><i class="fas fa-trash-alt"></i></button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

    @else

        <div class="text-center">
            <h2>Nothing found...</h2>
            <img class="my-4 w-75" src="/svg/empty.svg" alt="APP Ilustration">
        </div>

    @endif

@endsection