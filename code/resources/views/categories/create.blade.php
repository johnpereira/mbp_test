@extends('layouts.master')

@section('content')

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('index') }}">Home</a></li>
            <li class="breadcrumb-item"><a href="{{ route('categories.index') }}">Categories</a></li>
            <li class="breadcrumb-item active" aria-current="page">Add</li>
        </ol>
    </nav>

    <div class="card text-white bg-primary mb-3">
        <div class="card-header">Add the new category</div>
        <div class="card-body">
            <form action="{{ route('categories.store') }}" method="POST">
                @csrf
                <div class="form-group">
                    <input type="text" name="category" placeholder="Type the category name" class="form-control {{ $errors->has('category') ? 'is-invalid' : ''}}" autocomplete="off">
                    {!! $errors->first('category', '<div class="invalid-feedback text-light">:message</div>') !!}
                </div>
                <div class="form-group">
                    <input type="submit" value="ADD" class="btn btn-success btn-block">
                </div>
            </form>
        </div>
    </div>

@endsection