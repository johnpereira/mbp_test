@extends('layouts.master')

@section('content')

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('index') }}">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Credits</li>
        </ol>
    </nav>

    <h1>Credits</h1>

    <hr>

    <p><b>Author:</b> Jonathan Pereira <code>jp@jonathanpereira.com</code></p>

    <p><b>Framework:</b> Laravel 5.7</p>

    <p><b>Database:</b> MySql 5.7</p>

    <p><b>PHP:</b> 7.2.10</p>

    <p><b>Nginx:</b> 1.6.2</p>

    <hr>

    <h5 class="text-right">👨🏽‍💻 Paris, October 2018</h5>


@endsection