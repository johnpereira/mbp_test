@if($products->isNotEmpty())

    <form action="{{ route('delete-multiple-products') }}" method="POST" class="form-confirm" data-confirm-text="Proceed with the products deletion?">
        @csrf

        <div class="card border-primary mb-3">

            <div class="card-header">Products list</div>

            <div class="card-body text-primary">

                <div class="row">
                    <div class="col-6">
                        <button type="submit" class="delete-products-btn d-none btn btn-outline-danger">Delete selected</button>
                    </div>
                    <div class="col-6">
                        @if ($productsViewStyle == App\Models\Product::VIEW_STYLE_GROUPED )
                            <a class="float-right btn btn-outline-primary" href="{{ route('set-view-style', App\Models\Product::VIEW_STYLE_UNGROUPED) }}">Ungroup categories</a>
                        @else
                            <a class="float-right btn btn-outline-primary" href="{{ route('set-view-style', App\Models\Product::VIEW_STYLE_GROUPED) }}">Group by category</a>
                        @endif
                    </div>
                </div>

                <div class="custom-control custom-checkbox mt-3">
                    <input class="custom-control-input" type="checkbox" value="" id="select-all">
                    <label class="custom-control-label text-dark" for="select-all">
                        Select all
                    </label>
                </div>

            </div>

            <div class="products-list">

                @if ($productsViewStyle == App\Models\Product::VIEW_STYLE_GROUPED )

                    <ul class="list-group list-group-flush">
                        @foreach($products as $key => $group)
                            <li class="list-group-item">
                                <div class="custom-control custom-checkbox float-left">
                                    <input type="checkbox" class="custom-control-input select-all-category" data-slug="{{str_slug($key)}}" id="{{str_slug($key)}}">
                                    <label class="custom-control-label" for="{{str_slug($key)}}">
                                        <h5 class="text-primary">{{$key}}</h5>
                                    </label>
                                </div>
                            </li>
                            @foreach($group as $product)
                                <li class="list-group-item">
                                    <div class="custom-control custom-checkbox float-left ml-2">
                                        <input type="checkbox" name="productsIds[]" value="{{$product->id}}" class="custom-control-input checkbox-product {{str_slug($key)}}" id="product-{{ $product->id }}">
                                        <label class="custom-control-label" for="product-{{ $product->id }}">
                                            {{$product->product}}
                                        </label>
                                    </div>
                                    <a href="{{ route('delete-product', $product->id)  }}" class="float-right text-danger confirm" data-confirm-text="Are you sure?"><i class="far fas fa-trash-alt"></i></a>
                                </li>
                            @endforeach
                        @endforeach
                    </ul>

                @else

                    <ul class="list-group list-group-flush">
                        @foreach($products as $product)
                            <li class="list-group-item">
                                <div class="custom-control custom-checkbox float-left">
                                    <input type="checkbox" name="productsIds[]" value="{{$product->id}}" class="custom-control-input checkbox-product" id="product-{{ $product->id }}">
                                    <label class="custom-control-label" for="product-{{ $product->id }}">
                                        {{$product->product}} <i><small class="text-muted">({{ $product->category->category }})</small></i>
                                    </label>
                                </div>
                                <a href="{{ route('delete-product', $product->id)  }}" class="float-right text-danger confirm" data-confirm-text="Are you sure?"><i class="far fas fa-trash-alt"></i></a>
                            </li>
                        @endforeach
                    </ul>

                @endif

            </div>

            <div class="card-footer"></div>

        </div>

    </form>

@else

    <div class="text-center">
        <h2>Nothing found...</h2>
        <img class="my-4 w-75" src="/svg/empty.svg" alt="APP Ilustration">
    </div>

@endif