@extends('layouts.master')

@section('content')

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('index') }}">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Products list</li>
        </ol>
    </nav>

    <div class="card text-white bg-primary mb-3">
        <div class="card-header">Add the products</div>
        <div class="card-body">
            <form action="{{ route('store') }}" method="POST" >
                @csrf
                <div class="row">
                    <div class="col-sm-8">
                        <div class="form-group">
                            <input type="text" placeholder="+ New Product" name="product" value="{{ old('product') }}" class="form-control {{ $errors->has('product') ? 'is-invalid' : ''}}" autocomplete="off" tabindex="1">
                            {!! $errors->first('product', '<div class="invalid-feedback text-light">:message</div>') !!}
                        </div>
                        <div class="form-group">
                            <select name="category_id" id="category-select" class="form-control {{ $errors->has('category_id') ? 'is-invalid' : ''}}" tabindex="2">
                                <option value="">Select a category</option>
                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}" {{ old('category_id') == $category->id ? 'selected' : '' }}>{{ $category->category }}</option>
                                @endforeach
                                <option value="new" {{ old('category_id') == 'new' ? 'selected' : '' }}>__Add a new category</option>
                            </select>
                            {!! $errors->first('category_id', '<div class="invalid-feedback text-light">:message</div>') !!}
                        </div>
                        <div class="form-group">
                            <input type="text" id="new-category-field" name="category" placeholder="+ New Category" class="{{ (old('category_id') == 'new') ? '' : 'd-none' }} form-control {{ $errors->has('category') ? 'is-invalid' : ''}}" value="{{ old('category') }}" autocomplete="off" tabindex="3">
                            {!! $errors->first('category', '<div class="invalid-feedback text-light">:message</div>') !!}
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <input type="submit" value="ADD" class="btn btn-success btn-block" tabindex="4">
                    </div>
                </div>
            </form>
        </div>
    </div>

    <hr>

    @include('home.products')

@endsection