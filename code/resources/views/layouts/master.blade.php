<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Shopping List</title>

        <link rel="stylesheet" href="/css/app.css">

        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">

    </head>

    <body>

        <header>
            <!-- Fixed navbar -->
            <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
                <a class="navbar-brand" href="{{ route('index') }}">Shopping list</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarCollapse">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item @if(Request::is('/')) active @endif">
                            <a class="nav-link" href="{{ route('index') }}">Home</a>
                        </li>
                        <li class="nav-item @if(Request::is('categories', 'categories/*')) active @endif">
                            <a class="nav-link" href="{{ route('categories.index') }}">Categories</a>
                        </li>
                        <li class="nav-item @if(Request::is('credits')) active @endif">
                            <a class="nav-link" href="{{ route('credits')  }}">Credits</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>

        <!-- Begin page content -->
        <main role="main" class="container">
            <div class="row">

                <div class="col-4 d-none d-sm-block mt-4">
                    <h1>Instructions</h1>
                    <h4>Read carefully</h4>
                    <img class="my-4 w-75" src="/svg/app.svg" alt="APP Ilustration">
                    <p>So intuitive you don't need instructions. Enjoy!</p>
                </div>

                <div class="col-sm-8 mt-4">
                    @include('layouts.messages')
                    @yield('content')
                </div>

            </div>

        </main>

        <script src="/js/app.js"></script>
    </body>
</html>