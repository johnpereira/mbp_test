
$(document).ready(function(){
    addNewCategory();
    selectProducts();
    actionConfirm();
});

function addNewCategory()
{
    $('#category-select').change(function(){

        const categoryValue = $(this).val();

        if(categoryValue == 'new'){
            $('#new-category-field').removeClass('d-none');
        } else {
            $('#new-category-field').val('').addClass('d-none');
        }
    });
}

function selectProducts()
{
    $('#select-all').change(function(){
        const currentStatus = $(this).is(":checked");

        $('.products-list input').prop('checked', currentStatus);

        toggleDeleteBtn();
    });

    $('.select-all-category').change(function(){
        const currentStatus = $(this).is(":checked");

        $('.' + $(this).data('slug') ).prop('checked', currentStatus);

        toggleDeleteBtn();
    });

    $('.checkbox-product').change(function(){
        toggleDeleteBtn();
    });
}

function toggleDeleteBtn()
{
    if($('.checkbox-product:checked').length > 0){
        $('.delete-products-btn').removeClass('d-none');
    } else {
        $('.delete-products-btn').addClass('d-none');
    }
}

function actionConfirm()
{
    const confirmAction = function(){
        const confirmText = $(this).data('confirm-text');
        return confirm( confirmText );
    }

    $('.confirm').click(confirmAction);
    $('.form-confirm').submit(confirmAction);

}