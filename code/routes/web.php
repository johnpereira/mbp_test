<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('/', 'HomeController');

Route::resource('categories', 'CategoryController');

Route::get('/set-view-style/{productsViewStyle}', 'HomeController@setViewStyle')->name('set-view-style');

Route::get('/delete-product/{id}', 'HomeController@deleteProduct')->name('delete-product');

Route::post('/delete-multiple-products', 'HomeController@deleteMultipleProducts')->name('delete-multiple-products');

Route::get('/credits', function(){

   return view('credits.index');

})->name('credits');