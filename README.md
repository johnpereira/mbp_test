# First install
##Run PHP / Nginx / MySQL
docker-compose up -d

##Run composer install
docker-compose exec php composer install

##Run the migrations
docker-compose exec php php artisan migrate

## Update your hosts
```
sudo echo $(docker network inspect bridge | grep Gateway | grep -o -E '[0-9\.]+') "symfony.local" >> /etc/hosts
```

## Run the site
http://symfony.local/